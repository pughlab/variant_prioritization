## Config file for Prioritization of Variants script

## Locally using mac-fusion
base_dir <- "/Volumes/205.210.128.25/" ## OR "/mnt/work1/users/pughlab/" for running on cluster

sample_name = ""

drug_targets = c("","")  ## manually input gene names for drug targets
GO_IDs = c("","")        ## list of GO numbers (numerical chars only)
cancer_type = "OV"       ## TCGA Code
drug = ""                ## regex pattern of drugbank.ca accession #
purity =                 ## informatically or histologically imputed purity (0-1)
  
indel.file = ## path to annotated indel file (strelka or varscan2)
indel_tool = "strelka" ## OR varscan

snv.file = ## path to annotated snv file (Mutect)
  
output_dir = ## path to output dir
